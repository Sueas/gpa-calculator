package com.hfad.gpa_calculator;

/**
 * Created by sueas on 2018/5/12.
 */
public class Calculate {

    private static double gpa = 0;
    private static int poi = 0;
    public static int count = 0;
    private static double last = 0;
    public static boolean status = false;

    public static void setStatus(boolean stauts) {
        Calculate.status = stauts;
    }

    public static void setCount(int count) {
        Calculate.count = count;
    }

    public static boolean isStatus() {
        return status;
    }

    public double cal(String GPA, String point) {
        int p = 0;
        //if (count == 0 && GPA != null && point != null) {
        poi += Integer.parseInt(point);
        p += Integer.parseInt(point);
        if (GPA.equals("A+")) {

            gpa += 4.3 * p;
            count++;
        }
        if (GPA.equals("A0")) {
            gpa += 4.0 * p;
            count++;

        }
        if (GPA.equals("A-")) {
            gpa += 3.7 * p;
            count++;

        }
        if (GPA.equals("B+")) {
            gpa += 3.3 * p;
            count++;

        }
        if (GPA.equals("B0")) {
            gpa += 3.0 * p;
            count++;

        }
        if (GPA.equals("B-")) {
            gpa += 2.7 * p;
            count++;

        }
        if (GPA.equals("C+")) {
            gpa += 2.3 * p;
            count++;

        }
        if (GPA.equals("C0")) {
            gpa += 2.0 * p;
            count++;

        }
        if (GPA.equals("C-")) {
            gpa += 1.7 * p;
            count++;

        }
        if (GPA.equals("D+")) {
            gpa += 1.3 * p;
            count++;

        }
        if (GPA.equals("D0")) {
            gpa += 1.0 * p;
            count++;

        }
        if (GPA.equals("D-")) {
            gpa += 0.7 * p;
            count++;

        }
        if (GPA.equals("F")) {
            gpa += 0 * p;
            count++;
        }
        last = gpa / poi;
        last = (double) Math.round(last * 100) / 100;
        //   }


        return last;
    }

}