package com.hfad.gpa_calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

public class MainPageActivity extends AppCompatActivity {
    private static int mark = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpa_cal);
        final Spinner alphaSp = (Spinner) findViewById(R.id.alphabet);
        final Spinner detailSp = (Spinner) findViewById(R.id.detail);
        final Spinner pointSp = (Spinner) findViewById(R.id.point);
        final TextView result = (TextView) findViewById(R.id.text);
        ArrayList<Spinner> arrayList = new ArrayList<>();
        arrayList.add(alphaSp);
        arrayList.add(pointSp);
        arrayList.add(detailSp);
        SpinnerListener spinnerListenr = new SpinnerListener();
        spinnerListenr.listen(arrayList, result);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.addlayoutb, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ArrayList<LinearLayout> layouts = new ArrayList<>();
        ArrayList<TextView> textViews = new ArrayList<>();
        final TextView tv = (TextView) findViewById(R.id.text);
        final LinearLayout titles_ll = (LinearLayout) findViewById(R.id.test);
        final LinearLayout titles_ll1 = (LinearLayout) findViewById(R.id.test1);
        final LinearLayout titles_ll2 = (LinearLayout) findViewById(R.id.test2);
        final LinearLayout titles_ll3 = (LinearLayout) findViewById(R.id.test3);
        final LinearLayout titles_ll4 = (LinearLayout) findViewById(R.id.test4);
        final LinearLayout titles_ll5 = (LinearLayout) findViewById(R.id.test5);
        final ArrayList<Spinner> arrayList = new ArrayList<>();
        final SpinnerListener spinnerListener = new SpinnerListener();
        final ArrayList<Integer> arrayList1 = new ArrayList<>();
        textViews.add(tv);
        arrayList1.add(R.id.alphabet);
        arrayList1.add(R.id.point);
        arrayList1.add(R.id.detail);
        layouts.add(titles_ll);
        layouts.add(titles_ll1);
        layouts.add(titles_ll2);
        layouts.add(titles_ll3);
        layouts.add(titles_ll4);
        layouts.add(titles_ll5);

        switch (item.getItemId()) {
            case R.id.addlayout:
                Toast.makeText(this, "You clicked add key!", Toast.LENGTH_SHORT).show();
                for (int i = mark; i <= mark; i++) {

                    final LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.component, null);
                    ll.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
                    spinnerListener.listen(arrayList, tv);
                    // tv.setText("hello world");
                    layouts.get(i).addView(ll);
                }
                for (int j = 0; j < layouts.get(mark).getChildCount(); j++) {
                    final Spinner pointSp = (Spinner) layouts.get(mark).getChildAt(j).findViewById(R.id.point);
                    final Spinner alphaSp = (Spinner) layouts.get(mark).getChildAt(j).findViewById(R.id.alphabet);
                    final Spinner detialSp = (Spinner) layouts.get(mark).getChildAt(j).findViewById(R.id.detail);
                    arrayList.add(alphaSp);
                    arrayList.add(pointSp);
                    arrayList.add(detialSp);
                    spinnerListener.listen(arrayList,textViews.get(0));
                }
                mark++;
                return true;
            case R.id.droplayout:
                if (mark == 0)
                    Toast.makeText(this, "You can not delete option!", Toast.LENGTH_SHORT).show();
                else {
                    Toast.makeText(this, "You clicked drop key!", Toast.LENGTH_SHORT).show();
                    for (int i = mark; i >= mark; i--) {
                        layouts.get(i - 1).removeAllViews();
                    }
                    mark--;
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
