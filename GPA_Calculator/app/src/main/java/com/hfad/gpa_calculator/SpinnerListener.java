package com.hfad.gpa_calculator;

import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sueas on 2018/5/12.
 */

public class SpinnerListener {
    public static Boolean status = true;
    public static Boolean statusPoint = true;
    public static Boolean statusAlpha = true;
    private static String alphaStr;
    private static String pointStr;
    private static String detailStr;
    private static String GPA;
    private static int i = 0;
    private static int count = 0;

    public void listen(final ArrayList<Spinner> sp, TextView tv) {
        final ArrayList<Spinner> spinners = sp;
        final TextView result = tv;
        final Calculate cal = new Calculate();
        for (Spinner s : spinners) {
            count++;
            s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    //拿到被选择项的值
                    alphaStr = (String) spinners.get(0).getSelectedItem();
                    pointStr = (String) spinners.get(1).getSelectedItem();
                    detailStr = (String) spinners.get(2).getSelectedItem();
                    //把该值传给 TextView
                    if (alphaStr.equals("F")){
                        GPA = "F";
                    }else{
                        GPA = alphaStr + detailStr;
                    }
                    Double G = cal.cal(GPA, pointStr);
                    String grade = G.toString();
                    result.setText(grade);
                    GPA = null;
                    pointStr = null;
                    if (i == count) {
                        cal.setCount(0);
                        count = 0;
                        i = 0;
                    }
                    i++;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

        }


    }
}
